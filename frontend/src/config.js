const config = {
  backendUrl: process.env.NEXT_PUBLIC_BACKEND_HTTP_URL
};

export default config;
