import styles from '../styles/Home.module.css';

import Axios from 'axios';
import { useState } from 'react';

import ChallengeForm from '../components/ChallengeForm';
import ChallengeCard from '../components/ChallengeCard';

import config from '../config';

export async function getServerSideProps() {
  const url = `${config.backendUrl}/challenges`;
  const result = await Axios.get(url);
  const initialChallenges = result.data;

  return { props: { initialChallenges }};
}


export default function LandingPage({initialChallenges}) {
  const [currentChallenges, updateChallenges] = useState(initialChallenges);

  // function handler to rerender challengecards when a new challenge is added
  const addChallenge = (newChallenge) => {
    updateChallenges(currentChallenges.concat(newChallenge));
  }

  return (
    <div className={styles.mainContainer}>
      <h1>Challenge app</h1>
      <ChallengeForm addChallenge={addChallenge}/>
      {currentChallenges.map((challenge, index) => {
        return <ChallengeCard key={index} challengerName={challenge.challenger} challengeeName={challenge.challengee} series={challenge.series} freeText={challenge.freeText}/>
      })}
    </div>
  )
}
