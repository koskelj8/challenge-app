import axios from 'axios';

import styles from '../styles/ChallengeForm.module.css';
import { useState } from 'react';

import config from '../config';


export default function ChallengeForm({addChallenge}){

  const [challengerName, setChallengerName] = useState('');
  const [challengeeName, setChallengeeName] = useState('');
  const [challengeSeries, setSeries] = useState('');
  const [challengeFreeText, setFreeText] = useState('');

  const handleChallenger = (event) => {
    setChallengerName(event.target.value);
  }

  const handleChallengee = (event) => {
    setChallengeeName(event.target.value);
  }

  const handleSeries = (event) => {
    setSeries(event.target.value);
  }

  const handleFreeText = () => {
    setFreeText(event.target.value);
  }

  const submitChallenge = (event) => {
    event.preventDefault();

    // Reset form
    document.getElementById("challengeForm").reset();

    const url = `${config.backendUrl}/createChallenge`;
    axios.post(url, { challenger: challengerName, challengee: challengeeName, series: challengeSeries, freeText: challengeFreeText })
      .then(res => {
        console.log(res);
      })
    
    // Rerender challenges list
    addChallenge({ challenger: challengerName, challengee: challengeeName, series: challengeSeries, freeText: challengeFreeText});
  }

  return(
  <div className={styles.contentContainer}>
    <form id="challengeForm" className={styles.form} onSubmit={submitChallenge}>
      <div className={styles.input}>
        <label className={styles.label} htmlFor="challenger">Challenger:</label>
        <input type="text" name="challenger" onChange={handleChallenger}/>
      </div>
      <div className={styles.input}>
        <label className={styles.label} htmlFor="challengee">Challengee:</label>
        <input type="text" name="challengee" onChange={handleChallengee}/>
      </div>
      <div className={styles.input}>
        <label className={styles.label} htmlFor="series">Series:</label>
        <input type="text" name="series" onChange={handleSeries}/>
      </div>
      <div className={styles.input}>
        <label className={styles.label} htmlFor="freeText">Free Text:</label>
        <textarea className={styles.freeText} name="freeText" type="text" rows={6} columns={50} onChange={handleFreeText}></textarea>
      </div>
      <button className={styles.button} type="submit">Submit challenge</button>
    </form>
  </div>)
}

/*
  http://api.häfv.jaxu.fi
*/