import styles from '../styles/ChallengeCard.module.css';
import config from '../config';

export default function ChallengeCard({challengerName, challengeeName, series, freeText}) {
  return(
    <div className={styles.card}>
      
      <div className={styles.challenge}>
        <h2>{challengerName}</h2><h2>{challengeeName}</h2><h2>{series}</h2>
      </div>
      <div>
        <p className={styles.freeText}>
          {freeText}
        </p>
      </div>
    </div>
  )
}