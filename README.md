# Challenge app

## Infastructure

Caddy reverse proxy on azure. Dockerized frontend, backend, database and Caddy

### Dev
* backend: [https://api.challenge.jaxu.fi](https://api.challenge.jaxu.fi)
* frontend: [https://challenge.jaxu.fi](https://challenge.jaxu.fi)

## Frontend
* [Next JS](https://nextjs.org/)

### Prerequisites
* Node v14
* ```cd frontend && npm install```
* Copy ```.env.sample as .env.local```

### Commands
* ```npm run dev```: start development server
* ```npm run build```: Build frontend
* ```npm run start-prod```: Start production server

## Backend
* [Express](https://expressjs.com/)
* [Mongodb](https://mongodb.com/)
### Prerequisites
* Node v14
```cd backend && npm install```

### Commands
* ```npm run start```: Start development server on localhost:8000

## TODO:
* Convert to Typescript
* Gitlab CI/CD pipeline
* Telegram notification bot integration
* Better environment setup


