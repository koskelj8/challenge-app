#!/bin/sh

# Frontend
cd frontend

# Install dependencies
rm -rf node_modules
npm install

# Build production version
rm -rf .next/
npm run build


# Backend
cd ../backend

# install dependencies
rm -rf node_modules
npm install


