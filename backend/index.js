const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser')
const cors = require("cors");
const dotenv = require('dotenv');

dotenv.config();

const MongoClient = require('mongodb').MongoClient
const connectionString = 'mongodb+srv://jaxu:heevidatabase@hafv1.p5jyr.mongodb.net/<dbname>?retryWrites=true&w=majority'
//const connectionString = 'mongodb://172.17.0.1:27017/challenge-database';

const app = express();


MongoClient.connect(connectionString, {useUnifiedTopology: true}, (err, client) => {
  if (err) return console.error(err)
  console.log('Connected to Database')
  const db = client.db('challenge-database')
  const challengeCollection = db.collection('challenges')

  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  app.get('/', function(req, res) {
    res.send('Welcome to challenge app api');
  })

  app.get('/challenges', function(req, res) {
    challengeCollection.find().toArray()
    .then(results => {
      console.log(results)
      return res.status(200).json(results);
    })
    .catch(error => console.error(error))
  })

  app.post('/createChallenge', (req, res) => {
    console.log(req.body);
    challengeCollection.insertOne(req.body);

    // Send notification to telegram group
    const telegramUrl = `https://api.telegram.org/bot${process.env.TELEGRAM_BOT_TOKEN}/sendMessage`;
    const chatId = process.env.TELEGRAM_BOT_CHAT_ID;

    axios.post(telegramUrl, { "chat_id": chatId, "text": `New challenge between: ${req.body.challenger} and ${req.body.challengee}`});

    return res.status(201);
  })

  app.listen(8000, function() {
    console.log('listening on 8000')
  })
})
